package com.example.rtexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RtexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(RtexamApplication.class, args);
	}

}
