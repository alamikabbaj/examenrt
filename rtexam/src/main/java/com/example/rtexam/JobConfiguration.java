package com.example.rtexam;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Configuration
public class JobConfiguration {

    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step filePreparationTaskletStep){
        return new JobBuilder( "BillingJob",jobRepository)
                .start(filePreparationTaskletStep)
                .next(step1)
                .build();}



    @Bean
    public Step filePreparationTaskletStep(JobRepository jobRepository, JdbcTransactionManager transactionManager) {
        return new StepBuilder("filePreparationTaskletStep", jobRepository)
                .tasklet(new FilePreparationTasklet(),transactionManager)
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager,
                      ItemReader<Data> dataFileReader, ItemWriter<Data> dataTableWriter, ItemProcessor<Data, Data> dataProcessor, @Value("#{jobParameters['input.file']}") String inputFile) {

        RetryTemplate retryTemplate = new RetryTemplate();
        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(3000);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(3);
        retryTemplate.setRetryPolicy(retryPolicy);

        return new StepBuilder("fileReading", jobRepository)
                .<Data, Data>chunk((int) (countRows(inputFile)* 0.05) , transactionManager)
                .reader(dataFileReader)
                .processor(dataProcessor)
                .faultTolerant()
                .retry(Exception.class)
                .skipLimit(7)
                .writer(dataTableWriter)
                .build();
    }


    public int countRows(String filePath) {
        int rowCount = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            while (br.readLine() != null) {
                rowCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rowCount;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Data> dataFileReader(@Value("#{jobParameters['input.file']}") String inputFile){
        return new FlatFileItemReaderBuilder<Data>()
                .name("DataFileReader")
                .resource(new FileSystemResource(inputFile))
                .delimited()
                .names("data1", "data2", "data3")
                .targetType(Data.class)
                .build();
    }



    @Bean
    public JdbcBatchItemWriter<Data> dataTableWriter(DataSource dataSource, @Value("#{jobParameters['requete']}") String requete){
        return new JdbcBatchItemWriterBuilder<Data>()
                .dataSource(dataSource)
                .sql(requete)
                .beanMapped()
                .build();
    }


    @Bean
    @StepScope
    public ItemProcessor<Data, Data> dataProcessor() {
        return new DataTypeValidationProcessor();
    }




}
