package com.example.rtexam;

import org.springframework.batch.item.ItemProcessor;

public class DataTypeValidationProcessor implements ItemProcessor<Data, Data> {
    @Override
    public Data process(Data item) throws Exception {
        if (!isValidData(item)) {
            return null;
        }

        return item;
    }


    private boolean isValidData(Data data) {

        return true;
    }


}
